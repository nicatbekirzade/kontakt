<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%-- include header.jsp and set page title --%>
<jsp:include page="header.jsp">
    <jsp:param name="pageTitle" value="View phone page"/>
</jsp:include>

<h1><spring:message code="view.phones.main.header"/></h1>

<%-- Print model connectedUser --%>
<%--<h4><spring:message code="view.users.greetings"/> ${connectedUser} !</h4>--%>

<table id="usersTable">
    <thead>
    <tr>
        <th>id</th>
        <th>imei</th>
        <th>format</th>
        <th>blocked</th>
    </tr>

    </thead>
    <c:forEach items="${listView}" var="elements">

        <tr>
            <td id="elementId">${elements.id}</td>
            <td>${elements.imei}</td>

            <td class="format-button">
                <button>${elements.canFormat}</button>
            </td>
            <td class="block-button">
                <button id="blockedButton">${elements.blocked}</button>
            </td>
        </tr>

    </c:forEach>
</table>
<style>
    #usersTable {
        /*border: 1px solid black;*/
        margin: auto;
        width: 70%;
        border-collapse: collapse;
    }

    #usersTable td {
        border: 1px solid black;
        text-align: center;

    }

    #usersTable td:nth-child(4),
    #usersTable td:nth-child(3) {
        /*background-color: red;*/
        width: 100px;
    }

    #usersTable td button {
        width: 100%;
    }
</style>

<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
        crossorigin="anonymous"></script>

<script>

    // console.log($('.reserve-button'))
    $('.block-button').click(function (e) {
        let newBlocked;
        if (e.target.innerText === 'false') {
            newBlocked = true;
        } else {
            newBlocked = false;

        }
        // console.log(e.target.innerText)
        $.ajax({
            type: "PUT",
            url: "http://135.181.250.197:8087/api/phone/isBlock",
            // data: "blocked=false&id=5",
            contentType: "application/json",
            data: JSON.stringify({blocked: newBlocked, id: 1}),
            success: function (data) {
                e.target.innerText = newBlocked
            },
            // error: function (err) {
            //     console.log(err)
            // }
        });

    });
    // console.log($("#blockedButton").innerText)

</script>

<script>

    // console.log($('.reserve-button'))
    $('.format-button').click(function (e) {
        let newBlocked;
        if (e.target.innerText === 'false') {
            newBlocked = true;
        } else {
            newBlocked = false;

        }
        // console.log(e.target.innerText)
        $.ajax({
            type: "PUT",
            url: "http://135.181.250.197:8087/api/phone/canFormat",
            // data: "blocked=false&id=5",
            contentType: "application/json",
            data: JSON.stringify({canFormat: newBlocked, id: 1}),
            success: function (data) {
                e.target.innerText = newBlocked
            },
            // error: function (err) {
            //     console.log(err)
            // }
        });

    });
    // console.log($("#blockedButton").innerText)

</script>

<%-- include footer.jsp --%>
<jsp:include page="footer.jsp"/>