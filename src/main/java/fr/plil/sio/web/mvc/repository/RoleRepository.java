package fr.plil.sio.web.mvc.repository;

import fr.plil.sio.web.mvc.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {

    Role findByName(String name);
}
