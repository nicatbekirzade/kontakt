package fr.plil.sio.web.mvc.controller;

import fr.plil.sio.web.mvc.dto.BlockDto;
import fr.plil.sio.web.mvc.dto.FormatDto;
import fr.plil.sio.web.mvc.model.Phone;
import fr.plil.sio.web.mvc.service.PhoneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@RestController
@RequestMapping("/api/phone")
public class PhoneController {

    @Autowired
    private PhoneService phoneService;

    @PostMapping
    public Phone create(@RequestBody Phone phone) {

        return phoneService.create(phone);
    }

    @GetMapping(value = "/list-view")
    public ModelAndView listView() {
        ModelAndView model = new ModelAndView("viewPhone");
        model.addObject("listView", phoneService.findAll());
        return model;
    }

    @GetMapping(value = "/list")
    public List<Phone> listPhone() {
        return phoneService.findAll();
    }

    @PutMapping(value = "/isBlock", consumes = "application/json", produces = "application/json")
    public boolean isBlock(@RequestBody BlockDto blockDto) {

        return phoneService.block(blockDto);
    }

    @PutMapping(value = "/canFormat", consumes = "application/json", produces = "application/json")
    public boolean canFormat(@RequestBody FormatDto formatDto) {

        return phoneService.canFormat(formatDto);
    }


    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public class ResourceNotFoundException extends RuntimeException {
    }

//    @RequestMapping(value = "/api/users/{username}/", method = RequestMethod.GET)
//    public User listUsers(@PathVariable String username) {
//        User user = userService.findByUsername(username);
//        if (user == null) {
//            throw new ResourceNotFoundException();
//        } else {
//            return user;
//        }
//    }
}