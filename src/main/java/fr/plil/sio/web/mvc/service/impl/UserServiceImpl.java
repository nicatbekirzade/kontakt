package fr.plil.sio.web.mvc.service.impl;

import fr.plil.sio.web.mvc.model.UserK;
import fr.plil.sio.web.mvc.repository.RoleRepository;
import fr.plil.sio.web.mvc.repository.UserRepository;
import fr.plil.sio.web.mvc.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Slf4j
@Service("userService")
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    @Transactional
    public UserK createUser(String username, String password) {
        UserK user = new UserK();
        user.setUsername(username);
        user.setPassword(passwordEncoder.encode(password));
        user.getRoles().add(roleRepository.findByName("ROLE_USER"));
        userRepository.save(user);
        return user;
    }

    @Override
    public UserK findByUsername(String username) {
        log.info("username: "+username);
        return userRepository.findByUsername(username);
    }

    @Override
    public List<UserK> findAll() {
        return userRepository.findAll();
    }
}
