package fr.plil.sio.web.mvc.service.impl;

import fr.plil.sio.web.mvc.model.UserK;
import fr.plil.sio.web.mvc.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


@Slf4j
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserK user = userService.findByUsername(username);

        if (user == null) {
            log.error("user null");
            throw new UsernameNotFoundException("cannot found user " + username);
        }

        return user;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }
}