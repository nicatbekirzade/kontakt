package fr.plil.sio.web.mvc.service;

import fr.plil.sio.web.mvc.dto.BlockDto;
import fr.plil.sio.web.mvc.dto.FormatDto;
import fr.plil.sio.web.mvc.model.Phone;

import java.util.List;

public interface PhoneService {

    List<Phone> findAll();

    Phone create(Phone phone);

    boolean canFormat(FormatDto formatDto);

    boolean block(BlockDto blockDto);
}
