package fr.plil.sio.web.mvc.service;

public interface SecurityService {

    String findLoggedInUsername();
}