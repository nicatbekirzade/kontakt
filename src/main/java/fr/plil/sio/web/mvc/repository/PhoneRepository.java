package fr.plil.sio.web.mvc.repository;

import fr.plil.sio.web.mvc.model.Phone;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PhoneRepository extends JpaRepository<Phone, Long> {

    Optional<Phone> findById(Long id);
}
