package fr.plil.sio.web.mvc.controller;

import fr.plil.sio.web.mvc.service.SecurityService;
import fr.plil.sio.web.mvc.repository.UserRepository;
import fr.plil.sio.web.mvc.model.UserK;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
public class ViewUsersController {

    @Autowired
    private SecurityService securityService;

    @Autowired
    private UserRepository userRepository;

    @ModelAttribute("users")
    public List<UserK> populateUsers() {
        return userRepository.findAll();
    }

    @ModelAttribute("connectedUser")
    public String populateConnectedUser() { return securityService.findLoggedInUsername(); }

    @RequestMapping(value={"/"},method=RequestMethod.GET)
    public String getViewUsers() {
        return "viewUsers";
    }

    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void setSecurityService(SecurityService securityService) {
        this.securityService = securityService;
    }
}
