package fr.plil.sio.web.mvc.repository;

import fr.plil.sio.web.mvc.model.UserK;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<UserK, Long> {

    UserK findByUsername(String username);
}
