package fr.plil.sio.web.mvc.service;

import fr.plil.sio.web.mvc.model.UserK;

import java.util.List;

public interface UserService {

    UserK createUser(String username, String password);

    UserK findByUsername(String username);

    List<UserK> findAll();
}
