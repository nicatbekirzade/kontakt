package fr.plil.sio.web.mvc.service.impl;

import fr.plil.sio.web.mvc.dto.BlockDto;
import fr.plil.sio.web.mvc.dto.FormatDto;
import fr.plil.sio.web.mvc.exception.NotFoundException;
import fr.plil.sio.web.mvc.model.Phone;
import fr.plil.sio.web.mvc.repository.PhoneRepository;
import fr.plil.sio.web.mvc.service.PhoneService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service("phoneService")
public class PhoneServiceImpl implements PhoneService {

    @Autowired
    private PhoneRepository phoneRepository;

    @Override
    public List<Phone> findAll() {
        return phoneRepository.findAll();
    }

    @Override
    public boolean block(BlockDto blockDto) {
        Phone phone = phoneRepository.findById(blockDto.getId()).orElseThrow(() ->
                new NotFoundException("Phone with id: NotFound"));
        phone.setBlocked(blockDto.isBlocked());
        log.info("block");
        log.info(""+phone.isBlocked());
        phoneRepository.save(phone);
        return true;
    }

    @Override
    public boolean canFormat(FormatDto formatDto) {
        Phone phone = phoneRepository.findById(formatDto.getId()).orElseThrow(() ->
                new NotFoundException("Phone with id  NotFound"));
        phone.setCanFormat(formatDto.isCanFormat());
        log.info("format");
        log.info(""+ phone.isCanFormat());
        phoneRepository.save(phone);
        return true;
    }

    @Override
    public Phone create(Phone phone) {
        return phoneRepository.save(phone);
    }
}
